/*Se citeste un vector de nr complexe. Sa se gaseasca centrul si raza cercului
care contine in interior sau pe frontiera toate nr complexe.*/
#include <iostream>
using namespace std;

struct complex
{
	double re, img;
};

complex operator +(complex z1, complex z2)
{
	complex s;
	s.re = z1.re + z2.re;
	s.img = z1.img + z2.img;
	return s;
}

complex operator /(complex z1, double x)
{
	complex s;
	s.re = z1.re / x;
	s.img = z1.img / x;
	return s;
}
complex operator -(complex z1, complex z2)
{
	complex s;
	s.re = z1.re - z2.re;
	s.img = z1.img - z2.img;
	return s;
}
complex operator *(complex z1, complex z2)
{
	complex s;
	s.re = z1.re*z2.re - z1.img*z2.img;
	s.img = z1.re*z2.img + z1.img*z2.re;
	return s;
}
double operator!(complex z)
{
	double x;
	return x = z.re*z.re + z.img*z.img;
}
complex conjugat(complex z2)
{
	complex z;
	z.re = z2.re;
	z.img = -z2.img;
	return z;
}

complex operator/(complex z1, complex z2)
{
	complex z = z1 * conjugat(z2);
	return z / !z2;
}
istream& operator >>(istream&fl, complex z)
{
	return fl >> z.re >> z.img;
}
complex operator+=(complex&z1, complex z2)
{
	z1.re += z2.re;
	z1.img += z2.img;
	return z1;
}
complex operator -=(complex&z1, complex z2)
{
	z1.re -= z2.re;
	z1.img -= z2.img;
	return z1;
}
complex operator *=(complex&z1, complex z2)
{
	z1.re *= z2.re;
	z1.img *= z2.img;
	return z1;
}
complex operator >(complex z1, complex z2)
{
	if (z1.re > z2.re&&z1.img > z2.img)
		return z1;
	return z2;
}
complex operator <(complex z1, complex z2)
{
	if (z1.re < z2.re&&z1.img < z2.img)
		return z1;
	return z2;
}
complex operator <=(complex z1, complex z2)
{
	if (z1.re <= z2.re&&z1.img <= z2.img)
		return z1;
	return z2;
}
complex operator >=(complex z1, complex z2)
{
	if (z1.re >= z2.re&&z1.img >= z2.img)
		return z1;
	return z2;
}
bool operator ==(complex z1, complex z2)
{
	if (z1.re == z2.re&&z1.img == z2.img)
		return true;
	return false;
}
ostream& operator<<(ostream&fl, complex z)
{
	return fl << z.re << " " << z.img;
}
int main()
{
	complex z1, z2;
	z1.re = 11;
	z1.img = 32;
	z2.re = 1;
	z2.img = 38;
	complex y = z1 / z2;
	cout << y;
	system("pause");
	return 0;
}