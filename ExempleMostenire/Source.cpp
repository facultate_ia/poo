//#include <math.h> // pentru functia "fabs"
//#include <string.h> // pentru functia "strcpy"
//#include <iostream>
//#include <random>
//#include <stdlib.h>
//#include <time.h>
//#define PI 3.14159
//using namespace std;
//
//class figura // clasa abstracta, cu functii pur virtuale
//{
//protected:
//	char nume[20]; // denumirea figurii
//public: // functii pur virtuale
//	virtual float arie() = 0;
//	virtual float perimetru() = 0;
//	char* getnume()
//	{
//		return nume;
//	}
//};
//class dreptunghi : public figura
//{
//protected:
//	float x1, y1, x2, y2; // coordonate varfuri diagonal opuse
//public:
//	dreptunghi(float X1, float Y1, float X2, float Y2)
//	{
//		strcpy_s(nume, "Dreptunghi");
//		x1 = X1;
//		y1 = Y1;
//		x2 = X2;
//		y2 = Y2;
//	}
//	virtual float arie()
//	{
//		return fabs((x2 - x1) * (y2 - y1));
//	}
//	virtual float perimetru()
//	{
//		return 2 * (fabs(x2 - x1) + fabs(y2 - y1));
//	}
//};
//class patrat : public dreptunghi
//{
//protected:
//	float x, y, l; // coordonate vf. stanga-sus si lungime latura
//public:
//	patrat(float X, float Y, float L) : dreptunghi(X, Y, X + L, Y + L)
//	{ // constructorul patrat apeleaza dreptunghi
//		strcpy_s(nume, "Patrat");
//		x = X;
//		y = Y;
//		l = L;
//	}
//	virtual float perimetru()
//	{
//		return dreptunghi::perimetru();
//	}
//};
//class cerc : public figura
//{
//protected:
//	float x, y, r;
//public:
//	cerc(float x, float y, float r)
//	{
//		strcpy_s(nume, "Cerc");
//		this->x = x;
//		this->y = y;
//		this->r = r;
//	}
//	virtual float arie()
//	{
//		return PI * r * r;
//	}
//	virtual float perimetru()
//	{
//		return 2 * PI * r;
//	}
//};
//
//void qsort(int s, int d, figura** fig)
//{
//	int i = s, j = d, m = (s + d) / 2;
//	figura* figaux;
//	do
//	{
//		while (fig[i]->arie() < fig[m]->arie()) i++;
//		while (fig[j]->arie() > fig[m]->arie()) j--;
//		if (i <= j)
//		{
//			figaux = fig[i]; fig[i] = fig[j]; fig[j] = figaux;
//			if (i < m) i++;
//			if (j > m) j--;
//		}
//	} while (i < j);
//	if (s < m) qsort(s, m - 1, fig);
//	if (m < d) qsort(m + 1, d, fig);
//}
//
//int main()
//{
//	figura** fig;
//	int i, n;
//	srand(time(0));
//	cout << "Dati numarul de figuri:";
//	cin >> n;
//	fig = new figura * [n];
//	for (i = 0; i < n; i++)
//		switch (rand() % 3)
//		{
//		case 0: fig[i] = new patrat(rand() % 100, rand() % 100, rand() % 100 + 100);
//			break;
//		case 1: fig[i] = new dreptunghi(rand() % 100, rand() % 100, rand() % 100 + 100, rand() % 100 + 100);
//			break;
//		case 2: fig[i] = new cerc(rand() % 100, rand() % 100, rand() % 100 + 100);
//			break;
//		}
//	qsort(0, n - 1, fig);
//	cout << "Figurile sortate dupa arie:" << endl;
//	for (i = 0; i < n; i++)
//		cout << fig[i]->getnume() << " cu aria: " << fig[i]->arie() << endl;
//	for (i = 0; i < n; i++) delete[] fig[i];
//	delete[] fig;
//	return 0;
//}
//# include <math.h>
//# include <string.h>
//# include <stdlib.h>
//# include <iostream>
//# define PI 3.14159
//using namespace std;
//
//class figura
//{
//protected:
//	char nume[20]; // denumirea figurii
//public: // functii pur virtuale
//	virtual float arie() = 0;
//	virtual float perimetru() = 0;
//	char* getnume()
//	{
//		return nume;
//	}
//};
//class patrulater :public figura
//{
//protected:
//	float x1, y1, x2, y2, x3, y3, x4, y4;
//public:
//	patrulater(float X1, float Y1, float X2, float Y2,
//		float X3, float Y3, float X4, float Y4)
//	{
//		strcpy(nume, "Patrulater");
//		x1 = X1;
//		y1 = Y1;
//		x2 = X2;
//		y2 = Y2;
//		x3 = X3;
//		y3 = Y3;
//		x4 = X4;
//		y4 = Y4;
//	}
//	virtual float arie()
//	{
//		return fabs(x1 * y2 - x2 * y1 + x2 * y3 - x3 * y2 + x3 * y4 - x4 * y3 + x4 * y1 - x1 * y4) / 2;
//	}
//	virtual float perimetru()
//	{
//		return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) + sqrt((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2)) + sqrt((x4 - x3) * (x4 - x3) + (y4 - y3) * (y4 - y3)) + sqrt((x1 - x4) * (x1 - x4) + (y1 - y4) * (y1 - y4));
//	}
//};
//class dreptunghi : virtual public patrulater // clasa patrulater virtuala
//{
//protected:
//	float x1, y1, x2, y2;
//public:
//	dreptunghi(float X1, float Y1, float X2, float Y2) :
//		patrulater(X1, Y1, X2, Y1, X2, Y2, X1, Y2)
//	{
//		strcpy(nume, "Dreptunghi");
//		x1 = X1;
//		y1 = Y1;
//		x2 = X2;
//		y2 = Y2;
//	}
//	virtual float arie()
//	{
//		return fabs((x2 - x1) * (y2 - y1));
//	}
//	virtual float perimetru()
//	{
//		return 2 * (fabs(x2 - x1) + fabs(y2 - y1));
//	}
//};
//class romb : virtual public patrulater // clasa patrulater virtuala
//{
//protected:
//	float x, y, l, u;
//public:
//	romb(float X, float Y, float L, float U) :
//		patrulater(X, Y, X + L, Y, X + L * cos(U) + L, Y + L * sin(U), X + L * cos(U), Y + L * sin(U))
//	{
//		strcpy(nume, "Romb");
//		x = X;
//		y = Y;
//		l = L;
//		u = U;
//	}
//	virtual float arie()
//	{
//		return l * l * sin(u);
//	}
//	virtual float perimetru()
//	{
//		return 4 * l;
//	}
//};
//class patrat : public dreptunghi, public romb
//{
//protected:
//	float x, y, l;
//public:
//	patrat(float X, float Y, float L) :dreptunghi(X, Y, X + L, Y + L), romb(X, Y, L, PI / 2), patrulater(X, Y, X + L, Y, X + L, Y + L, X, Y + L)
//	{
//		strcpy(nume, "Patrat");
//		x = X;
//		y = Y;
//		l = L;
//	}
//	virtual float arie() // calcul arie patrat ca fiind dreptunghi
//	{
//		return dreptunghi::arie();
//	}
//	virtual float perimetru() // calcul perimetru patrat ca fiind romb
//	{
//		return romb::perimetru();
//	}
//};
//void main(void)
//{
//	patrulater P(10, 10, 100, 40, 110, 100, 20, 30);
//	dreptunghi d(10, 20, 200, 80);
//	romb r(20, 50, 100, PI / 3);
//	patrat p(20, 10, 100);
//	cout << "Aria patrulaterului: " << P.arie() << endl;
//	cout << "Perimetrul patrulaterului: " << P.perimetru() << endl << endl;
//	cout << "Aria dreptunghiului: " << d.arie() << endl;
//	cout << "Perimetrul dreptunghiului: " << d.perimetru() << endl << endl;
//	cout << "Aria rombului: " << r.arie() << endl;
//	cout << "Perimetrul rombului: " << r.perimetru() << endl << endl;
//	cout << "Aria patratului: " << p.arie() << endl;
//	cout << "Perimetrul patratului: " << p.perimetru() << endl << endl;
//}
//#include <iostream>
//
//class B
//{
//public:
//	B()
//	{
//		std::cout << "Const B";
//	}
//	virtual ~B()
//	{
//		std::cout << "Deconst B";
//	}
//};
//class D :public B
//{
//public:
//	D()
//	{
//		std::cout << "Const D";
//	}
//	~D()
//	{
//		std::cout << "Deconst D";
//	}
//};
//int main()
//{
//	B* p = new D;
//	delete p;
//	return 0;
//}
#include <conio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;
template <class T>
struct NodListaSimpluInlantuita
{
	T info;
	NodListaSimpluInlantuita<T>* leg;
};
template <class T>
class Stiva
{
private:
	NodListaSimpluInlantuita<T>* cap;
public:
	Stiva() // constructor
	{
		cap = NULL;
	}
	int operator!() // verificare stiva goala
	{
		return cap == NULL;
	}
	void operator<<(T x) // introducere in stiva
	{
		NodListaSimpluInlantuita<T>* p = cap;
		cap = new NodListaSimpluInlantuita<T>;
		cap->info = x;
		cap->leg = p;
	}
	void operator>>(T&); // scoatere din stiva
	friend ostream& operator<<(ostream&, Stiva<T>&);
	// tiparire continut stiva
	void golire(); // golire stiva
	~Stiva() // destructor
	{
		golire();
	}
};
template <class T>
void Stiva<T>::operator>>(T& x)
{
	if (cap == NULL) throw "Stiva goala!";
	x = cap->info;
	NodListaSimpluInlantuita<T>* p = cap->leg;
	delete cap;
	cap = p;
}
template <class T>
ostream& operator<<(ostream& fl, Stiva<T>& st)
{
	NodListaSimpluInlantuita<T>* p = st.cap;
	while (p != NULL)
	{
		fl << p->info;
		p = p->leg;
	}
	return fl;
}
template <class T>
void Stiva<T>::golire()
{
	NodListaSimpluInlantuita<T>* p = cap;
	while (cap != NULL)
	{
		p = cap->leg;
		delete cap;
		cap = p;
	}
}
void main()
{
	char x;
	Stiva<char> st;
	do
	{
		cout << endl << endl;
		cout << " 1. Introducere element in stiva" << endl;
		cout << " 2. Scoatere element din stiva" << endl;
		cout << " 3. Afisare continut stiva" << endl;
		cout << " 4. Golire stiva" << endl << endl;
		cout << "Esc - Parasire program" << endl << endl;
		switch (_getch())
		{
		case '1':
			cout << "Dati un caracter: " << flush;
			st << _getche();
			break;
		case '2':
			try
			{
				st >> x;
				cout << "Am scos din stiva: " << x;
			}
			catch (char* mesajeroare)
			{
				cerr << "Eroare: " << mesajeroare;
			}
			break;
		case '3':
			if (!st) cout << "Stiva este goala!";
			else cout << "Stiva contine: " << st;
			break;
		case '4':
			st.golire();
			cout << "Stiva a fost golita!";
			break;
		case 27: return;
		default:
			cerr << "Trebuie sa apasati 1,2,3,4 sau Esc";
		}
		cout.flush();
		_getch();
	} while (1);
}