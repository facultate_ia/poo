#include <iostream>
#include <time.h>
#include <random>
using namespace std;

class AnimalCompanie
{
public:
	virtual int NrPicioare() = 0;
	virtual bool AreBlana() = 0;
};

class Caine :public AnimalCompanie
{
public:
	virtual int NrPicioare()
	{
		return 4;
	}
	virtual bool AreBlana()
	{
		return true;
	}
};

class Sarpe :public AnimalCompanie
{
public:
	virtual int NrPicioare()
	{
		return 0;
	}
	virtual bool AreBlana()
	{
		return false;
	}
};

class Tarantula :public AnimalCompanie
{
public:
	virtual int NrPicioare()
	{
		return 6;
	}
	virtual bool AreBlana()
	{
		return false;
	}
};

int main()
{
	srand(time(0));
	AnimalCompanie** vector;
	int n;
	cout << "Introduceti nr de animale din vector:";
	cin >> n;
	vector = new AnimalCompanie * [n];
	for (int index = 0; index < n; index++)
	{
		switch (rand() % 3)
		{
		case 0:
			vector[index] = new Caine;
			break;
		case 1:
			vector[index] = new Sarpe;
			break;
		case 2:
			vector[index] = new Tarantula;
			break;
		default:
			break;
		}
	}
	int nrTarantule = 0;
	for (int index = 0; index < n; index++)
	{
		if (dynamic_cast<Tarantula*>(vector[index]))
			nrTarantule++;
	}
	cout << "Numarul de tarantule din vector este " << nrTarantule;
	for (int index = 0; index < n; index++)
	{
		delete[]vector[index];
	}
	delete[]vector;
	return 0;
}