#include <iostream>
#include <time.h>
#include <random>
using namespace std;

class Vehicul
{
public:
	virtual int NrRoti() = 0;
	virtual bool AreMotor() = 0;
};

class Autoturism :public Vehicul
{
public:
	virtual int NrRoti()
	{
		return 4;
	}
	virtual bool AreMotor()
	{
		return true;
	}
};

class Bicicleta :public Vehicul
{
public:
	virtual int NrRoti()
	{
		return 2;
	}
	virtual bool AreMotor()
	{
		return false;
	}
};

class Caruta :public Vehicul
{
public:
	virtual int NrRoti()
	{
		return 4;
	}
	virtual bool AreMotor()
	{
		return false;
	}
};

int main()
{
	srand(time(NULL));
	Vehicul** vector;
	int n = rand() % 5 + 5;
	cout << n;
	vector = new Vehicul * [n];
	for (int index = 0; index < n; index++)
	{
		switch (rand() % 3)
		{
		case 0:
			vector[index] = new Autoturism;
			break;
		case 1:
			vector[index] = new Bicicleta;
			break;
		case 2:
			vector[index] = new Caruta;
			break;
		default:
			break;
		}
	}
	int nr = 0;
	for (int index = 0; index < n; index++)
	{
		if (dynamic_cast<Bicicleta*>(vector[index]))
			nr++;
	}
	cout << "Nr de biciclete: " << nr << endl;
	for (int index = 0; index < n; index++)
	{
		delete[]vector[index];
	}
	delete[]vector;
	return 0;
}