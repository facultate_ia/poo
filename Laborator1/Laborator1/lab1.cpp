#include <iostream>
#include <algorithm>
#include <cstring>
#include <iomanip>
using namespace std;
struct student
{
	char Nume[50];
	float media;
};
void citire(int &numarDeStudenti, student *&vector)
{
	cin >> numarDeStudenti;
	vector = new student[numarDeStudenti];
	for (int index = 0; index < numarDeStudenti; index++)
	{
		cin.getline(vector[index].Nume, 50);
		cin.getline(vector[index].Nume, 50);
		cin >> vector[index].media;
	}
}
bool comparator(student x, student y)
{
	return strcmp(x.Nume, y.Nume) < 0;
}
void afisare(int numarDeStudenti, student *vector)
{
	for (int i = 0; i < 66; i++)
		cout << "-";
	cout << endl;
	cout << setw(65) << setfill(' ') << "|N crt|Nume" << "|Nota|";
	cout << endl;
	for (int i = 0; i < 66; i++)
	{
		if (i == 0 || i == 6 || i == 57 || i == 65) cout << "|";
		else
			cout << "-";
	}
	cout << endl;
	float media = 0;
	for (int i = 0; i < numarDeStudenti; i++)
	{
		cout << "|" << setw(5) << setiosflags(ios::right) << i + 1 << setw(1) << "|" << resetiosflags(ios::right) << setw(50) << setiosflags(ios::left) << vector[i].Nume << setw(1) << resetiosflags(ios::left) << "|" << setw(7) << fixed << setprecision(2) << setiosflags(ios::right | ios::showpoint) << vector[i].media << setw(1) << resetiosflags(ios::right) << "|\n";
		media = media + vector[i].media;
	}
	media /= numarDeStudenti;
	for (int i = 0; i < 66; i++)
	{
		if (i == 0 || i == 57 || i == 65) cout << "|";
		else
			cout << "-";
	}
	cout << endl;
	cout << "|" << setw(56) << setiosflags(ios::left) << "Medie:" << resetiosflags(ios::left) << setw(1) << "|" << setw(7) << media << setw(1) << "|\n";
	for (int i = 0; i < 66; i++)
		cout << "-";
	cout << endl;
}
int main()
{
	int numarDeStudenti;
	student *vector;
	citire(numarDeStudenti, vector);
	sort(vector, vector + numarDeStudenti, comparator);
	afisare(numarDeStudenti, vector);
	system("pause");
	return 0;

}