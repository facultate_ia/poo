#include <iostream>
using namespace std;
struct Multime
{
	int n, e[1000];
};
int cautSecv(int x, int n, int *a)
{
	for (int i = 0; i < n; i++)
		if (x == a[i]) 
			return 1;
	return 0;
}
//Multime operator +(Multime a, Multime b)
//{
//	Multime c;
//	for (int i = 0; i < a.n; i++) 
//		c.e[i] = a.e[i];
//	c.n = a.n;
//	for (int i = 0; i < b.n; i++)
//		if (!cautSecv(b.e[i], a.n, a.e))
//			c.e[c.n++] = b.e[i];
//	return c;
//}
Multime operator +(Multime a, int x)
{
	Multime b;
	for (int i = 0; i < a.n; i++) 
		b.e[i] = a.e[i];
	b.n = a.n;
	if (!cautSecv(x, b.n, b.e)) 
		b.e[b.n++] = x;
	return b;
}
void intercls(int m, int *a, int n, int *b, int *c)
{
	int i = 0, j = 0, k = 0, l;
	while (i < m && j < n)
		if (a[i] < b[j]) 
			c[k++] = a[i++];
		else 
			c[k++] = b[j++];
	for (l = i; l < m; l++) 
		c[k++] = a[l];
	for (l = j; l < n; l++)
		c[k++] = b[l];
}
void sortIntercls(int s, int d, int *a, int *b)
{
	if (s == d) 
	{
		b[0] = a[s];
		return; 
	}
	int m = (s + d) / 2, *a1, *a2;
	a1 = new int[m - s + 1];
	a2 = new int[d - m];
	sortIntercls(s, m, a, a1);
	sortIntercls(m + 1, d, a, a2);
	intercls(m - s + 1, a1, d - m, a2, b);
	delete[] a1;
	delete[] a2;
}
int cautBin(int x, int s, int d, int *a)
{
	if (s == d)
	{
		if (a[s] == x) 
			return 1;
		else 
			return 0;
	}
	int m = (s + d) / 2;
	if (a[m] == x) 
		return 1;
	if (x < a[m])
		return cautBin(x, s, m, a);
	return cautBin(x, m + 1, d, a);
}
Multime operator +(Multime a, Multime b)
{
	int i;
	Multime c;
	if (a.n < b.n)
	{
		if (a.n)
			sortIntercls(0, a.n - 1, a.e, c.e);
		c.n = a.n;
		for (i = 0; i < b.n; i++)
			if (!cautBin(b.e[i], 0, a.n - 1, c.e))
				c.e[c.n++] = b.e[i];
	}
	else
	{
		if (b.n)
			sortIntercls(0, b.n - 1, b.e, c.e);
		c.n = b.n;
		for (i = 0; i < a.n; i++)
			if (!cautBin(a.e[i], 0, b.n - 1, c.e))
				c.e[c.n++] = a.e[i];
	}
	return c;
}
ostream& operator <<(ostream& fl, Multime& a)
{
	for (int i = 0; i < a.n; i++) 
		fl << a.e[i] << " ";
	return fl;
}
istream& operator >>(istream& fl, Multime& a)
{
	fl >> a.n;
	for (int i = 0; i < a.n; i++) 
		fl >> a.e[i];
	return fl;
}
void main()
{
	int x;
	Multime a, b, c;
	cout << "Dati prima multime:" << endl;
	cin >> a;
	cout << "Dati a doua multime:" << endl;
	cin >> b;
	cout << "Dati un numar intreg:";
	cin >> x;
	c = a + b + x;
	cout << "Reuniunea este: " << c << endl;
}