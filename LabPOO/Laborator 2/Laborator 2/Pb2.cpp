/*Sa se scrie o functie care primeste ca parametrii datele despre o matrice de dimensiuni n*m.
Se cere sa se returneze prin parametrii datele despre o noua matrice care are linii din
prima matrice a caror suma a elementelor este pozitiva.*/
#include<iostream>
#include<fstream>
#include<algorithm>
using namespace std;

struct matr
{
	int ordinLinie, suma;
};

void citire(int **&matrice, int &m, int &n)
{
	ifstream f("fisierPb2.txt");
	f >> m >> n;
	matrice = new int*[m];
	for (int i = 0; i < m; i++)
		matrice[i] = new int[n];
	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
			f >> matrice[i][j];
}

void afisare(int **mat, int m, int n)
{
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
			cout << mat[i][j] << "\t ";
		cout << "\n";
	}
}

void dealocare(int **&matrice, int m)
{
	for (int i = 0; i < m; i++)
		delete[] matrice[i];
	delete[] matrice;
}

int sumaLinie(int **matrice, int m, int n, int linie)
{
	int suma = 0;
	for (int i = 0; i < n; i++)
		suma = suma + matrice[linie][i];
	return suma;
}

bool comparator(matr elem1, matr elem2)
{
	return(elem1.suma < elem2.suma);
}

void creare(int **matrice, int m, int n, matr nouaMatr[10], int &k)
{
	//nouaMatr = new matr[m];
	int s;
	k = 0;
	for (int i = 0; i < m; i++)
	{
		s = sumaLinie(matrice, m, n, i);
		if (s > 0)
		{
			nouaMatr[k].ordinLinie = i;
			nouaMatr[k].suma = s;
			k++;
		}
	}
	sort(nouaMatr, nouaMatr + k, comparator);
}

void afisareStruct(matr *nouaMatr, int dim)
{
	for (int i = 0; i < dim; i++)
		cout << nouaMatr[i].ordinLinie << " " << nouaMatr[i].suma << "\n";
}

void matricePoz(int m, int n, int **matrice, int &m2, int **&b)
{
	struct temp
	{
		int *p;
		int s;
	}*t;
	m2 = 0;
	t = new temp[m];
	int s;
	for (int i = 0; i < m; i++)
	{
		s = 0;
		for (int j = 0; j < m; j++)
			s += matrice[i][j];
		if (s > 0)
			t[m2].p = matrice[i], t[m2++].s = s;
	}
	b = new int*[m2];
	for (int i = 0; i < m2; i++)
		b[i] = t[i].p;
}

int main()
{
	int **matrice = NULL, m, n, k;
	//matr *nouaMatr = NULL;
	matr nouaMatr[10];
	citire(matrice, m, n);
	afisare(matrice, m, n);
	creare(matrice, m, n, nouaMatr, k);
	afisareStruct(nouaMatr, k);
	dealocare(matrice, m);
	system("pause");
	return 0;
}