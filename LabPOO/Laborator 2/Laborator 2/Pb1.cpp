#include<iostream>
using namespace std;

//Mod 1
#define matralloc1(a,tipdate,m,n)\
a=new tipdate[m];\
for(int i=0;i<m;i++)\
a[i]=new tipdate[n];

#define freematr1(a,m)\
for(int i=0;i<m;i++)\
delete[]a[i];\
delete[] a;

//Mod 2
#define matralloc2(a,tipdate,m,n)\
a=new tipdate[m];\
a[0]=new tipdate [m*n];\
for(int i=1;i<m;i++)\
a[i] = a[i - 1] + n;

#define freematr2(a)\
delete[]a[0];\
delete[] a;

int main()
{
	system("pause");
	return 0;
}