#include<iostream>

using namespace std;

double f(double x, double y)
{
	if (y == 0)
		throw "impartirea prin 0";
	if (x / y <= 0)
		throw x / y;
	return log(x / y);
}

int main()
{
	double a, b;
	cin >> a >> b;
	try
	{
		double c = f(a, b);
		cout << c << endl;
	}
	catch (const char *mesaj)
	{
		cerr << mesaj << endl;
	}
	catch (double v)
	{
		cerr << "Nr negativ sau 0: " << v << endl;
	}
	system("pause");
	return 0;
}


