#include<iostream>
using namespace std;

class complex
{
private:
	double re, im;
public:
	void setRe(double x);
	void setIm(double x);
	double GetRe();
	double GetIm();
	complex conj();
	complex operator+(complex);
};

void complex::setRe(double x)
{
	re = x;
}

void complex::setIm(double x)
{
	im = x;
}

double complex::GetRe()
{
	return re;
}

double complex::GetIm()
{
	return im;
}

complex complex::conj()
{
	complex z;
	z.re = re;
	z.im = -im;
	return z;
}

complex complex::operator+(complex z2)
{
	complex z;
	z.re = re + z2.re;
	z.im = im + z2.im;
	return z;
}

int main()
{
	complex z1, z2;
	z1.setRe(1);
	z2.setRe(2);
	z1.setIm(3);
	z2.setIm(4);
	cout << z1.GetRe() << " " << z1.GetIm() << "\n" << z2.GetRe() << " " << z2.GetIm() << "\n";
	complex z = z1 + z2;
	cout << z.GetRe() << " " << z.GetIm() << "\n";
	system("pause");
	return 0;
}