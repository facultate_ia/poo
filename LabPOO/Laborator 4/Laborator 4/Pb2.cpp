//Se citeste un vector de nr complexe. Sa se gaseasca centrul si raza cercului care contine toate nr complexe.
#include<iostream>
#include<fstream>
using namespace std;

struct complex
{
	float re, im;
};

ostream& operator <<(ostream &fl, complex z)
{
	return fl << z.re << " " << z.im << "\n";
}

void citire(complex *&vector, int &dim)
{
	ifstream f("fis.txt");
	f >> dim;
	vector = new complex[dim];
	for (int i = 0; i < dim; i++)
		f >> vector[i].re >> vector[i].im;
	f.close();
}

float aria(complex x, complex y, complex z)
{
	return (x.re*y.im + y.re*z.im + x.im*z.re - z.re*y.im - x.re*z.im - y.re*x.im);
}

void afisare(complex *vector, int dim)
{
	for (int i = 0; i < dim; i++)
		cout << vector[i];
}

void dealoc(complex *&vector)
{
	delete[]vector;
}

int main()
{
	complex *vector = NULL;
	int dim;
	citire(vector, dim);
	afisare(vector, dim);
	cout << aria(vector[0], vector[1], vector[2]);
	dealoc(vector);
	system("pause");
	return 0;
}