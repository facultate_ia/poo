#include<iostream>
using namespace std;

struct complex
{
	double re, im;
};

ostream& operator <<(ostream &fl, complex z)
{
	return fl << z.re << " " << z.im << "\n";
}

complex operator +(complex z1, complex z2)
{
	complex s;
	s.re = z1.re + z2.re;
	s.im = z1.im + z2.im;
	return s;
}

complex operator -(complex z1, complex z2)
{
	complex diferenta;
	diferenta.re = z1.re - z2.re;
	diferenta.im = z1.im - z2.im;
	return diferenta;
}

complex operator *(complex z1, complex z2)
{
	complex produs;
	produs.re = z1.re* z2.re - z1.im* z2.im;
	produs.im = z1.re* z2.im + z1.im * z2.re;
	return produs;
}

complex operator /(complex z1, complex z2)
{
	complex impartire;
	impartire.re = (z1.re * z2.re + z1.im*z2.im) / (z2.re*z2.re + z2.im*z2.im);
	impartire.im = (z1.im*z2.re - z1.re*z2.im) / (z2.re*z2.re + z2.im*z2.im);
	return impartire;
}

complex operator +(complex z1, float z2)//adunarea unui numar complex cu unul real cu ajutorul supraincarcarii de operatori
{
	complex s;
	s.re = z1.re + z2;
	s.im = z1.im;
	return s;
}

complex operator +=(complex &z1, complex z2)
{
	z1.re += z2.re;
	z1.im += z2.im;
	return z1;
}

int main()
{
	complex z1, z2, suma, dif, produs, impartire;
	z1.re = 1; z1.im = 3;
	z2.re = 2; z2.im = 4;
	suma = z1 + z2;
	cout << suma;
	dif = z1 - z2;
	cout << dif;
	produs = z1 * z2;
	cout << produs;
	impartire = z1 / z2;
	cout << impartire;
	complex pam = z1 + 4;
	cout << pam;
	z1 += z2;
	cout << z1;
	system("pause");
	return 0;
}
