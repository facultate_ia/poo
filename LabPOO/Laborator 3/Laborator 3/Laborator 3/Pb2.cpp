//Sa se scrie o functie template care caclueaza suma, produsul si determinantul unei matrici.

#include<iostream>
using namespace std;

template<class T>
void citireMatrice(T **&matrice, int &n)
{
	cout << "n="; cin >> n;
	matrice = new T*[n];
	for (int i = 0; i < n; i++)
		matrice[i] = new T[n];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			cin >> matrice[i][j];
}

template<class T>
void afisareMatrice(T **matrice, int n)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			cout << matrice[i][j] << " ";
		cout << endl;
	}
	cout << endl;
}

template<class T>
void dealocareMatrice(T **matrice, int n)
{
	for (int i = 0; i < n; i++)
		delete[] matrice[i];
	delete[] matrice;
}

template<class T>
void sumaMatrice(T **matrice1, T **matrice2, int n)
{
	T **suma = NULL;
	suma = new T*[n];
	for (int i = 0; i < n; i++)
		suma[i] = new T[n];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			suma[i][j] = matrice1[i][j] + matrice2[i][j];
	afisareMatrice(suma, n);
	dealocareMatrice(suma, n);
}

template<class T>
void produsMatrice(T **matrice1, T **matrice2, int n)
{
	T **produs = NULL;
	produs = new T*[n];
	for (int i = 0; i < n; i++)
		produs[i] = new T[n];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			produs[i][j] = 0;
	for (int k = 0; k < n; k++)
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
			{
				produs[i][j] += matrice1[i][k] * matrice2[k][j];
			}
	afisareMatrice(produs, n);
	dealocareMatrice(produs, n);
}

int main()
{
	int **matrice1 = NULL, **matrice2 = NULL, n;
	citireMatrice(matrice1, n);
	citireMatrice(matrice2, n);
	afisareMatrice(matrice1, n);
	afisareMatrice(matrice2, n);
	sumaMatrice(matrice1, matrice2, n);
	produsMatrice(matrice1, matrice2, n);
	dealocareMatrice(matrice1, n);
	dealocareMatrice(matrice2, n);
	system("pause");
	return 0;
}