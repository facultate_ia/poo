#include<iostream>
#include<cmath>
using namespace std;

void citire(double *vector, int &dim)
{
	//cout << "dim= ";
	//cin >> dim;
	vector = new double[dim];
	for (int i = 0; i < dim; i++)
		cin >> vector[i];
}

void afisare(double *vector, int dim)
{
	for (int i = 0; i < dim; i++)
		cout << vector[i] << " ";
	cout << endl;
}
void dealoc(double *vector, int dim)
{
	delete[]vector;
}

double l(double *v1, double *v2, const unsigned n = 2, const double *alfa = NULL, const double k = 2)
{
	double s = 0;
	if (alfa == NULL)
	{
		for (unsigned i = 0; i < n; i++)
			s += pow(abs(v1[i] - v2[i]), k);
	}
	else
	{
		for (unsigned i = 0; i < n; i++)
			s += pow(abs(v1[i] - v2[i]), k)*alfa[i];
	}
	return pow(s, 1 / k);
}

int main()
{
	double *vector1 = NULL, *vector2 = NULL, *alfa = NULL, k = 2;
	int n=2;
	citire(vector1, n);
	citire(vector2, n);
	citire(alfa, n);
	afisare(vector1, n);
	afisare(vector2, n);
	afisare(alfa, n);
	cout << l(vector1, vector2, n, alfa, k) << endl;
	dealoc(vector1, n);
	dealoc(vector2, n);
	dealoc(alfa, n);
	system("pause");
	return 0;
}