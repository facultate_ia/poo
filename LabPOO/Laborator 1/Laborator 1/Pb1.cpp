#include<iostream>
#include<iomanip>
using namespace std;

struct student
{
	char nume[100];
	int nota;
};

void citire(int &n, student lista[100])
{
	cout << "Se da numarul de studenti: ";
	cin >> n;
	cout << endl;
	cout << "Se citeste numele si nota: ";
	cout << endl;
	for (int i = 0; i < n; i++)
	{
		cin >> lista[i].nume;
		cin >> lista[i].nota;
	}
}

void alfabet(int n, student lista[100])
{
	student aux;
	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
			if (strcmp(lista[i].nume, lista[j].nume) > 0)
			{
				aux = lista[i];
				lista[i] = lista[j];
				lista[j] = aux;
			}
}

void afisare1(int n, student lista[100])
{
	for (int i = 0; i < 62; i++)cout << "-";
	cout << "\n";

	cout << "|";
	cout << setw(4);
	cout << setiosflags(ios::right);
	cout << "Nr.";

	cout << "|";
	cout << setw(50);
	cout << resetiosflags(ios::right);
	cout << setiosflags(ios::left);
	cout << "Nume";

	cout << "|";
	cout << setw(4);
	cout << resetiosflags(ios::left);
	cout << setiosflags(ios::right);
	cout << "Nota";
	cout << "|";

	cout << "\n";
	for (int i = 0; i < 62; i++)cout << "-";
	cout << "\n";
	alfabet(n, lista);

	for (int i = 0; i < n; i++)
	{
		cout << "|";
		cout << setw(4);
		cout << setiosflags(ios::right);
		cout << i + 1;

		cout << "|";
		cout << setw(50);
		cout << resetiosflags(ios::right);
		cout << setiosflags(ios::left);
		cout << lista[i].nume;

		cout << "|";
		cout << setw(4);
		cout << resetiosflags(ios::left);
		cout << setiosflags(ios::right);
		cout << lista[i].nota;
		cout << "|";

		cout << "\n";
	}
	for (int i = 0; i < 62; i++)cout << "-";
	cout << "\n";
}

void afisare2(int n, student lista[100])
{
	int suma = 0;
	float media;
	for (int i = 0; i < n; i++) suma = suma + lista[i].nota;
	media = (float)suma / n;

	cout << "|";
	cout << setw(55);
	cout << resetiosflags(ios::right);
	cout << setiosflags(ios::left);
	cout << "Medie:";

	cout << "|";
	cout << setw(4);
	cout << setprecision(2);
	cout << resetiosflags(ios::left);
	cout << setiosflags(ios::right);
	cout << media;
	cout << "|";

	cout << "\n";
	for (int i = 0; i < 62; i++)cout << "-";
}

int main()
{
	int n;
	student lista[100];
	citire(n, lista);
	afisare1(n, lista);
	afisare2(n, lista);
	system("pause");
	return 0;
}