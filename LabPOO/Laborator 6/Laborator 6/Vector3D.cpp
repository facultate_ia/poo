#include "Vector3D.h"

Vector3D::Vector3D() {
	this->x = 0;
	this->y = 0;
	this->z = 0;
}

Vector3D::Vector3D(double X, double Y, double Z) :x(X), y(Y), z(Z) {
}

//Vector3D::Vector3D(Vector3D &copie)
//{
//	this->x = copie.x;
//	this->y = copie.y;
//	this->z = copie.z;
//}

istream& operator>>(istream& in, Vector3D &v) {
	in >> v.x >> v.y >> v.z;
	return in;
}

ostream& operator<<(ostream& out, Vector3D& v) {
	out << v.x << " " << v.y << " " << v.z << endl;
	return out;
}

Vector3D Vector3D::operator+(Vector3D v) {
	return Vector3D(this->x + v.x, this->y + v.y, this->z + v.z);
}

Vector3D operator-(Vector3D v1, Vector3D v2) {
	return Vector3D(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

double Vector3D::operator!() {
	return sqrt(this->x*this->x + this->y * this->y + this->z * this->z);
}

Vector3D Vector3D::operator*(int s) {
	return Vector3D(s*this->x, s*this->y, s*this->z);
}

Vector3D operator*(int s, Vector3D v) {
	return v * s;
}

double Vector3D::cos(Vector3D v) {
	//double cosi= (this->x*v.x + this->y*v.y + this->z + v.z) / (!(*this) * !v);
	double cosi, produsScalar = this->x*v.x + this->y*v.y + this->z + v.z, norma = !(*this) * !v;
	//cout << produsScalar << " " << norma << endl;
	cosi=produsScalar/norma;
	return cosi;
}