#include "Vector3D.h"
#include<fstream>

void citire(Vector3D vec[], int &n) {
	ifstream f("fisVector3D.txt");
	f >> n;
	for (int i = 0; i < n; i++)
		f >> vec[i];
	f.close();
}

int paraleli(Vector3D vec[], int n, int &poz2) {
	float cosMin = -1, poz1 = 0;
	poz2 = 0;
	for (int i = 0; i < n - 1; i++)
		for (int j = i+1; j < n; j++)
			if (vec[i].cos(vec[j]) > cosMin)
			{
				cosMin = vec[i].cos(vec[j]);
				cout << cosMin << endl;
				poz1 = i;
				poz2 = j;
			}
	return poz1;
}

int main()
{
	Vector3D v1, v2, suma, dif;
	//cin >> v1;
	//cin >> v2;

	//suma = v1 + v2;// sau v=v1.operator+(v2);
	//cout << "Suma celor doi vectori este: " << suma << endl;

	//dif = v1 - v2;
	//cout << "Diferenta celor doi vectori este: " << dif << endl;;

	//cout << "Norma vectorului 1 este: " << !v1 << endl;

	//Vector3D inm = v1 * 2;//v1.operator(2);
	//cout << "Inmultirea cu un scalar: " << inm << endl;// inmultit la dreapta

	//inm = 2 * v1;//operator*(2,v1);
	//cout << inm << endl;

	//se citesc n vectori sa se afiseze cei mai aproape de a fi paraleli 2 vect(cos cat mai aproape de 1)

	Vector3D vec[20];
	int n, poz2 = 0, poz1;
	citire(vec, n);
	poz1 = paraleli(vec, n, poz2);
	cout << vec[poz1] << vec[poz2];

	system("pause");
	return 0;
}