#pragma once
#include<iostream>
#include<cmath>
using namespace std;

class Vector3D
{
private:
	double x, y, z;
public:
	Vector3D();
	Vector3D(double, double, double);
	//Vector3D(Vector3D&);

	friend istream& operator>>(istream&, Vector3D&);
	friend ostream& operator<<(ostream&, Vector3D&);
	Vector3D operator+(Vector3D);
	friend Vector3D operator-(Vector3D, Vector3D);
	double operator!();
	Vector3D operator*(int);//la dreapta
	friend Vector3D operator*(int, Vector3D);
	double cos(Vector3D);
};