#include "complex.h"

complex::complex()
{
}

complex::~complex()
{
}

void complex::SetRe(double x)
{
	re = x;
}

void complex::SetIm(double x)
{
	im = x;
}

double complex::GetRe()
{
	return re;
}

double complex::GetIm()
{
	return im;
}

complex complex::conj()
{
	complex z;
	z.re = re;
	z.im = -im;
	return z;
}

complex complex::operator+(complex z2)
{
	complex z;
	z.re = re + z2.re;
	z.im = im + z2.im;
	return z;
}