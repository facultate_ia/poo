#pragma once
class complex
{
private:
	double re, im;
public:
	complex();
	~complex();
	void SetRe(double x);
	void SetIm(double x);
	double GetRe();
	double GetIm();
	complex conj();
	complex operator+(complex);
};

