#include <iostream>
using namespace std;

double f(double x, double y)
{
	if (!y)
		throw "Impartire la 0";
	if (x / y <= 0)
		throw x / y;
	return log(x / y);
}

int main()
{
	double a, b;
	cin >> a >> b;
	double c;
	try {
		c = f(a, b);
		cout << c;
	}
	catch (const char *mesaj) {
		cerr << mesaj << endl;
	}
	catch (double v) {
		cerr << v << endl;
	}
	system("pause");
	return 0;
}