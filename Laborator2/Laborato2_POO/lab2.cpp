#include <iostream>
using namespace std;

#define matralloc1(a,tipdate,m,n)\
a=new tipdate*[m];\
for(int i=0;i<m;i++)\
	a[i]=new tipdate[n];

#define matrfree1(a,m)\
for(int i=0;i<m;i++)\
	delete[] a[i];\
delete[] a;

#define matralloc2(a,tipdate,m,n)\
a=new tipdate*[m];\
a[0] = new tipdate[m*n];\
for(int i=1;i<m;i++)\
	a[i]=a[i-1]+n;

#define matrfree2(a)\
delete[] a[0];\
delete[] a;

//Transmitere prin referinta
void inc(int &x)
{
	x++;
}
//Fara referinta
void inc(int *x)
{
	(*x)++;
}

int main()
{
	int *p = new int;
	delete p;

	int n;
	cin >> n;
	int *a = new int[n];
	delete[] a;

	int m, n;
	int **a;
	//matralloc1(a, int, m, n);

	//2) tipul referinta
	int v1;
	int &v2 = v1;//au aceeasi adresa de memorie | alias

	struct A
	{
		int a;
	};
	struct B
	{
		A a;
	};
	B b;
	int &c = b.a.a;
	c = 1; //b.a.a = 1;

	/*int a = 1;
	inc(a);
	inc(a);*/
	/*Sa se scrie o functie care primeste ca parametrii datele despre o matrice de dimensiuni n*m.
	Se cere sa afiseze */
	
	system("poause");
	return 0;
}