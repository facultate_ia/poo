/*Sa se scrie o functie care primeste ca parametrii datele despre o matrice de dimensiuni n*m.
Se cere sa se returneze prin parametrii datele despre o noua matrice care are linii din 
prima matrice a caror suma a elementelor este pozitiva.*/
#include <iostream>
#include <algorithm>
using namespace std;

struct matr_suma
{
	int suma;
	int *pointer;
};

int suma(int *vector, int n)
{
	int sum = 0;
	for (int i = 0; i < n; i++)
		sum += vector[i];
	return sum;
}

bool comparator(matr_suma a, matr_suma b)
{
	return a.suma > b.suma;
}

void matrpoz(int m, int n, int **a, int &m2, int **&b)
{
	matr_suma *matrice = new matr_suma[m];
	int k=0;
	for (int i = 0; i < m; i++)
	{
		int s = suma(a[i], n);
		if(s>0)
		{
			matrice[k].pointer = a[i];
			matrice[k++].suma = s;
		}
	}
	sort(matrice, matrice + k, comparator);
	b = new int*[k];
	for (int i = 0; i < k; i++)
		b[i] = matrice[i].pointer;
	delete[] matrice;
}

void citire(int m, int n, int **&matrice)
{
	cin >> n >> m;
	matrice = new int*[m];
	for (int i = 0; i < m; i++)
		matrice[i] = new int[n];
	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
			cin >> matrice[i][j];
}

int main()
{

	system("pause");
	return 0;
}