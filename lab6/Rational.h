#pragma once
#include <iostream>
using namespace std;
class Rational
{
	int nr, num;
	void simplif();
public:
	Rational() {}
	Rational(int nr, int num = 1);
	Rational operator+(Rational);
	friend Rational operator-(Rational, Rational);
	friend istream& operator>>(istream&, Rational&);
	friend ostream& operator<<(ostream&, Rational);
	Rational operator=(Rational);
};