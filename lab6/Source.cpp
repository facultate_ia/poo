#include <iostream>
#include "Rational.h"
using namespace std;

	void Rational::simplif()
	{
		int a = nr, b = num, r;
		for (; b; r = a % b, a = b, b = r);
		if (!a)throw "fractie nedeteminata";
		nr /= a; 
		num /= a;
	}
	Rational::Rational(int nr, int num)
	{
		if (!num)throw "numitor";
		this->nr = nr;
		this->num = num;
		simplif();
	}
	Rational Rational::operator+(Rational r2)
	{
		return Rational(nr*r2.num + r2.nr*num, num*r2.num);
	}
	Rational operator-(Rational r1, Rational r2)
	{
		return Rational(r1.nr*r2.num - r2.nr*r1.num, r1.num + r2.num);
	}
	istream& operator>>(istream&fl, Rational &r)
	{
		return fl >> r.nr >> r.num;
	}
	ostream& operator<<(ostream&fl, Rational r)
	{
		return fl << r.nr << " " << r.num;
	}
int main()
{
	Rational a(5,2), b(6,4), c;
	c = a + b;
	cout << c;
	system("pause");
	return 0;
}